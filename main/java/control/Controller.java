package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.channels.ShutdownChannelGroupException;

import javax.swing.JOptionPane;

import vedere.View;

public class Controller {
	View view = new View();
    Simulation s;
	public Controller(View v,Simulation s) {
		this.view = v;
        this.s=s;
		this.view.listenerButon(new StartListener());
		this.view.serviceMaxListener(new ServiceMaxListener());
		this.view.serviceMinListener(new ServiceMinListener());
		this.view.timpMaxListener(new TimeMaxListener());
		this.view.timpMinListener(new TimeMinListener());

	}
	
	class StartListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			//System.out.println(view.getNrCozi());
			view.adaugaCoada(view.getNrCozi());
			s = new Simulation(view.getNrCozi(),view);
			try {
			s.setMinArrivalTime(view.minTime());
			s.setMaxArrivalTime(view.maxTime());
			s.setMinProccessingTime(view.minServiceTime());
			s.setMaxProccessingTime(view.maxServiceTime());
			
			s.setTimeLimit(view.durataSim());
			s.setNumberOfServers(view.getNrCozi());
			//s.setNumberOfClients(view.nrClienti());
			//System.out.println("Arrival time min : " + s.getMinArrivalTime()+"iar cel maxim : "+s.getMaxArrivalTime());
			//System.out.println("Proccesing time min : "+s.getMinProccessingTime()+"iar cel maxim : "+s.getMaxProccessingTime());
			//System.out.println("durata :"+s.getTimeLimit());
			//System.out.println("clienti : "+s.getNumberOfClients()+"case : "+s.getNumberOfServers());
			}
			catch(NumberFormatException excep) {
				JOptionPane.showMessageDialog(view,
						"Te rog sa completezi toate campurile!" + excep.getMessage(), "EROARE",
						JOptionPane.INFORMATION_MESSAGE);
			}
          Thread t = new Thread(s);
          t.start();
		}

	}

	class ServiceMaxListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			view.setServiceMax(null);
		}

		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class ServiceMinListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			view.setServiceMin(null);

		}

		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class TimeMaxListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			view.setMaxTime(null);

		}

		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class TimeMinListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
			view.setMinTime(null);
		}

		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}

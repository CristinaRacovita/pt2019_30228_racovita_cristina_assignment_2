package control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

import model.ComparatorTimpTask;
import model.Scheduler;
import model.Server;
import model.Task;
import vedere.View;

public class Simulation implements Runnable {

	private int timeLimit;
	private int maxProccessingTime;
	private int minProccessingTime;
	private int minArrivalTime;
	private int maxArrivalTime;
	private int numberOfServers;
	private int numberOfClients=0;

	private Scheduler schelduler;
	public ArrayList<Task> generateTasks;
	private ArrayList<Server> servers = new ArrayList<Server>(numberOfServers);
	View v = new View();
	// private Thread t;

	public Simulation(int numberOfServers,View v) {
		this.v=v;
		schelduler = new Scheduler(numberOfServers,v);
		
		//generateNRandomTasks(numberOfClients);

	}

	public int getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}

	public int getMaxProccessingTime() {
		return maxProccessingTime;
	}

	public void setMaxProccessingTime(int maxProccessingTime) {
		this.maxProccessingTime = maxProccessingTime;
	}

	public int getMinProccessingTime() {
		return minProccessingTime;
	}

	public void setMinProccessingTime(int minProccessingTime) {
		this.minProccessingTime = minProccessingTime;
	}

	public int getMinArrivalTime() {
		return minArrivalTime;
	}

	public void setMinArrivalTime(int minArrivalTime) {
		this.minArrivalTime = minArrivalTime;
	}

	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}

	public void setMaxArrivalTime(int maxArrivalTime) {
		this.maxArrivalTime = maxArrivalTime;
	}

	public int getNumberOfServers() {
		return numberOfServers;
	}

	public void setNumberOfServers(int numberOfServers) {
		this.numberOfServers = numberOfServers;
	}


	private void generateNRandomTasks() {
		for (int i = 0; i < 10000; i++) {
			int randomId = ThreadLocalRandom.current().nextInt(0, 50);
			//System.out.println("minimarrival e "+minArrivalTime+"maxArrival e "+maxArrivalTime);
			int randomArrivalTime = ThreadLocalRandom.current().nextInt(minArrivalTime, maxArrivalTime);
			int randomProccessingTime = ThreadLocalRandom.current().nextInt(minProccessingTime, maxProccessingTime);
			Task t = new Task();
			t.setId(randomId);
			t.setArrivalTime(randomArrivalTime);
			t.setProcessingTime(randomProccessingTime);
			//System.out.println("Trebuie sa generez "+ numberOfClients);
			//System.out.println("Eu sunt "+ (i+1) + " client generat!");
			// System.out.println("Task id :" + t.getId()+ " arrival time : "
			// +t.getArrivalTime()+" proccessing : "+t.getProcessingTime());
			generateTasks.add(t);
		}
		Collections.sort(generateTasks, new ComparatorTimpTask());
	}

	public Task getSmaller(int currentTime) {
		Task t = null;
		Collections.sort(generateTasks, new ComparatorTimpTask());
		for (Task w : generateTasks) {
			if (w.getArrivalTime() == currentTime) {
				t = w;
				break;
			}
		}
		generateTasks.remove(t);
		return t;
	}

	public void run() {
		generateTasks = new ArrayList<Task>(numberOfClients);
		generateNRandomTasks();
		int currentTime = 0;
		while (currentTime < timeLimit) {
			//System.out.println(currentTime);
            //v.text.append(currentTime+"\n");
			Task t = getSmaller(currentTime);
			schelduler.dispatchTask(t);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
			}

			currentTime++;
		}
		
	}

}

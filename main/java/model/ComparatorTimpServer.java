package model;

import java.util.Comparator;

public class ComparatorTimpServer implements Comparator<Server> {

	public int compare(Server s1, Server s2) {
		return s1.getWaitingPeriod().intValue()-s2.getWaitingPeriod().intValue();
	}

}

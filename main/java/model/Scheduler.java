package model;

import java.util.ArrayList;
import java.util.Collections;

import vedere.View;

public class Scheduler {

	private ArrayList<Server> servers;
	private int maxNoServers;
	View v = new View();
	public Scheduler(int maxNoServers,View v) {
		servers = new ArrayList<Server>();
		this.v=v;
		for(int i=0;i<maxNoServers;i++) {
			servers.add(new Server(i+1,v));			

			//new Thread(servers.get(i)).start();
			
		}
	}
	
	public void dispatchTask(Task t) {
		Collections.sort(servers, new ComparatorTimpServer());
		
		//System.out.println("casa : " + servers.get(0).nr + " cu timpul : "+ servers.get(0).getWaitingPeriod() +"\ncasa : " + servers.get(1).nr + " cu timupl : "+ servers.get(1).getWaitingPeriod() +"\ncasa : " + servers.get(2).nr + " cu timupl : "+ servers.get(2).getWaitingPeriod());
	    //clienti++;
		servers.get(0).addTask(t);		

	}
	
}

package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import vedere.View;

public class Server implements Runnable {
	private BlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod;
	private AtomicInteger isRun;
	String s1 = new String();
	String s2 = new String();
	public int nr;
	private Thread thread;
	View v = new View();

	public Server(int nr,View v) {
		this.v=v;
		tasks = new ArrayBlockingQueue<Task>(100);
		waitingPeriod = new AtomicInteger(0);
		isRun = new AtomicInteger(1); 
		this.nr=nr;
		thread = new Thread(this);
		thread.start();
	}

	public Task getTask() throws InterruptedException{
	
			Task t = tasks.take();
			Thread.sleep( t.getProcessingTime() * 1000 );
			waitingPeriod.set(waitingPeriod.intValue() - t.getProcessingTime());
			String s = v.nrCoziText.get(nr-1).getText();
			v.nrCoziText.get(nr-1).setText( s.substring(7));
			return t;
	}

	public void addTask(Task t) {
		try {
			tasks.add(t);
			waitingPeriod.set( waitingPeriod.intValue() + t.getProcessingTime() );
			
			//System.out.println("\nDVdskldnd " +  waitingPeriod.intValue());
			
			System.out.println("Clientul "+t.getId()+" a venit la : "+t.getArrivalTime()+", cu proccesing time : "+t.getProcessingTime()+" la casa : "+nr);
			s2="Clientul "+t.getId()+" a venit la : "+t.getArrivalTime()+", cu proccesing time : "+t.getProcessingTime()+" la casa : "+nr+"\n";
			v.text.append(s2);
			v.nrCoziText.get(nr-1).setText( v.nrCoziText.get(nr-1).getText() + " client" );
			Thread.sleep(1000);

		} catch (Exception e) {
			return;
		}
	}

	public void run() {
		while (true) {	
			try {
				Task t= getTask();
				
				//Thread.sleep( t.getProcessingTime() * 1000 );
				System.out.println("Clientul "+t.getId()+" a plecat la : "+t.getFinishTime()+", cu proccesing time : "+t.getProcessingTime()+" la casa : "+nr);
				s1="Clientul "+t.getId()+" a plecat la : "+t.getFinishTime()+", cu proccesing time : "+t.getProcessingTime()+" la casa : "+nr+"\n";
				v.text.append(s1);
				//waitingPeriod.set(waitingPeriod.get() - t.getArrivalTime() - t.getFinishTime());

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (isRun.get() == 0) {
				break;
			}
		}

	}

	public void setRun(int value) {
		isRun.set(value);
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

}

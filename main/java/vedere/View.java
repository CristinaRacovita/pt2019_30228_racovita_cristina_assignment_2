package vedere;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame {

	JLabel service = new JLabel("Timp de servire : ");
	JLabel timpul = new JLabel("Timp sosire : ");
	JLabel cozi = new JLabel("Numar cozi : ");
	//JLabel clienti = new JLabel("Numar clienti : ");
	JLabel simulare = new JLabel("Interval de simulare : ");
	JTextField serviceMin = new JTextField(5);
	JTextField serviceMax = new JTextField(5);

	JTextField timpMin = new JTextField(20);
	JTextField timpMax = new JTextField(20);
	JTextField coziText = new JTextField(20);
	//JTextField clientiText = new JTextField(20);
	JTextField sim = new JTextField(20);
	JButton buton = new JButton("START");

	public JTextArea text = new JTextArea(50, 100);
	JPanel panel = new JPanel();
	public ArrayList<JTextField> nrCoziText = new ArrayList<JTextField>(100);
	ArrayList<JLabel> etichete = new ArrayList<JLabel>(100);

	public View() {

		this.setTitle("Cozi Supermarket");
		this.setSize(2000, 1000);
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);

		Font font = new Font("SansSerif",Font.BOLD,20);
		text.setFont(font);
		timpMin.setText("MIN");
		timpMax.setText("MAX");

		serviceMax.setText("MAX");
		serviceMin.setText("MIN");

		JPanel p1 = new JPanel();
		p1.setLayout(new GridBagLayout());

		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();
		JPanel p8 = new JPanel();
		JPanel p9 = new JPanel();
		JPanel p10 = new JPanel();
		JPanel p11 = new JPanel();
		JPanel p12 = new JPanel();
		JPanel p13 = new JPanel();

		p5.add(service);
		p3.add(serviceMax);
		p4.add(serviceMin);
		p2.add(p4);
		p2.add(p3);
		p2.setLayout(new BoxLayout(p2, BoxLayout.X_AXIS));
		p5.add(p2);

		p8.add(timpMax);
		p9.add(timpMin);
		p7.add(p9);
		p7.add(p8);
		p7.setLayout(new BoxLayout(p7, BoxLayout.X_AXIS));
		p5.add(timpul);
		p5.add(p7);

		p10.add(coziText);
		p11.add(sim);
		
		JPanel p17 = new JPanel();
		
		//p17.add(clientiText);

		p12.add(cozi);
		p12.add(p10);
		p12.setLayout(new BoxLayout(p12, BoxLayout.PAGE_AXIS));
		p5.add(p12);

		//JPanel p16 = new JPanel();
		//p16.add(clienti);
		//p16.add(p17);
		//p16.setLayout(new BoxLayout(p16, BoxLayout.PAGE_AXIS));
        //p5.add(p16);
		
		p13.add(simulare);
		p13.add(p11);
		p13.setLayout(new BoxLayout(p13, BoxLayout.PAGE_AXIS));
		p5.add(p13);

		p5.setLayout(new BoxLayout(p5, BoxLayout.Y_AXIS));

		
		JPanel pan2 = new JPanel();

		text.setEditable(false);
		//textCozi.setEditable(false);

		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new GridLayout(0,2));
		//pan1.add(panel);
		
		//text.setLayout(new BorderLayout());
		text.setColumns(50);
		pan2.add(text);
       
		JPanel p15 = new JPanel();
		p15.add(buton);
		p5.add(p15);
		
		panel.setSize(new Dimension(30,10));

		this.setLayout(new BorderLayout());
		this.add(p5, BorderLayout.LINE_START);
		this.add(panel, BorderLayout.CENTER);
		this.add(pan2, BorderLayout.LINE_END);
		//this.pack();
		//this.add(p6);

	}

	public void adaugaCoada(int nrCozi) {
		for (int i = 0; i < nrCozi; i++) {
			etichete.add(new JLabel("Coada nr " + (i + 1) + " : "));
			nrCoziText.add(new JTextField(20));
			panel.add(etichete.get(i));
			panel.add(nrCoziText.get(i));
		}
		panel.revalidate();
		panel.repaint();
	}

	public void listenerButon(ActionListener action) {
		buton.addActionListener(action);
	}

	public int getNrCozi() {
		return Integer.parseInt(coziText.getText());
	}

	public int durataSim() {
		return Integer.parseInt(sim.getText());
	}

	public int minServiceTime() {
		return Integer.parseInt(serviceMin.getText());
	}

	public int maxServiceTime() {
		return Integer.parseInt(serviceMax.getText());
	}

	public int minTime() {
		return Integer.parseInt(timpMin.getText());
	}

	public int maxTime() {
		return Integer.parseInt(timpMax.getText());
	}

	public void setMaxTime(String s) {
		timpMax.setText(s);
	}

	public void setMinTime(String s) {
		timpMin.setText(s);
	}

	public void setServiceMax(String s) {
		serviceMax.setText(s);
	}

	public void setServiceMin(String s) {
		serviceMin.setText(s);
	}

	public void serviceMinListener(MouseListener l) {
		serviceMin.addMouseListener(l);
	}

	public void serviceMaxListener(MouseListener l) {
		serviceMax.addMouseListener(l);
	}

	public void timpMinListener(MouseListener l) {
		timpMin.addMouseListener(l);
	}

	public void timpMaxListener(MouseListener l) {
		timpMax.addMouseListener(l);
	}

}
